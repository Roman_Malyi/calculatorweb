import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Step} from '../../models/step';
import {Option} from '../../models/option';
import {OptionDto} from '../../models/option-dto';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.sass']
})
export class StepsComponent implements OnInit {
  steps: Step[];
  rootOption: Option;
  previousOptions: OptionDto[];
  sumEstimate: number;
  constructor(private http: HttpClient) {
    this.sumEstimate = 0;
    this.rootOption = null;
  }
  ReceiveOption( optionDto: OptionDto) {
    this.sumEstimate = 0;
    if ( optionDto.isRoot) {
      alert('isRoot');
      if (optionDto.increasesCost) {
        this.rootOption = optionDto.option;
      } else {
        alert('Root null');
        this.rootOption = null;
      }
    } else {
      alert('!isRoot');
      if (optionDto.increasesCost) {
        this.previousOptions[optionDto.orderNumber] = optionDto;
      } else {
        this.previousOptions[optionDto.orderNumber] = null;
      }
    }
    if (this.rootOption != null) {
      alert('calculateAll');
      this.calculateAll();
    }
  }
  calculateAll() {
    for (const i in this.steps) {
      if (this.previousOptions[i] !== undefined) {
        for (const value of this.previousOptions[i].option.optionValues) {
          if (value.relatedId = this.rootOption.id) {
            this.sumEstimate += this.rootOption.optionValues[0].maxValue * value.maxValue;
            break;
          }
        }
      }
    }
  }
  ngOnInit() {
    this.http.get('http://localhost:8001/steps').subscribe((data: Step[]) => this.steps = data);
  }
}
