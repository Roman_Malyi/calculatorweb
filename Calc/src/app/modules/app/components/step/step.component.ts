import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {Step} from '../../models/step';
import {Option} from '../../models/option';
import {OptionDto} from '../../models/option-dto';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.sass']
})
export class StepComponent implements OnInit {

  @Input() step: Step;
  selectedOption: Option;
  @Input() sumEstimate: number;
  @Output() SendOption = new EventEmitter<OptionDto>();
  private readonly clickOption: OptionDto;

  sendOption(option: OptionDto) {
    this.SendOption.emit(option);
  }
  constructor() {
    this.clickOption =  new OptionDto();
    this.selectedOption = null;
  }
  ngOnInit() {
  }
  onSelect(option: Option): void {
    this.clickOption.option = this.selectedOption;
    this.clickOption.isRoot = this.step.isStepRoot;
    this.clickOption.orderNumber = this.step.orderNumber - 1; // -1?
    if (this.selectedOption !== option) {
      alert('increases');
      if (this.selectedOption !== null) {
        alert('selectedOption !null');
        this.clickOption.increasesCost = false;
        this.sendOption(this.clickOption);
      }
      this.selectedOption = option;
      this.clickOption.option = this.selectedOption;
      this.clickOption.increasesCost = true;
      this.sendOption(this.clickOption);
    } else {
      this.clickOption.increasesCost = false;
      this.sendOption(this.clickOption);
      this.selectedOption = null;
    }
  }
}
