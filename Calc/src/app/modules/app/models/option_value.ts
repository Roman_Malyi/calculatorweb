export class OptionValue {
  public id: number;
  public optionId: number;
  public relatedId: number;
  public minValue: number;
  public maxValue: number;
}
