import {OptionValue} from './option_value';
export class Option {
  public id: number;
  public name: string;
  public stepId: number;
  public optionValues: OptionValue[];
}

