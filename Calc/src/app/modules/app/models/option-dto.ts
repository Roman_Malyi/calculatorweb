import {Option} from './option';

export class OptionDto {
  public option: Option;
  public isRoot: boolean;
  public increasesCost: boolean;
  public orderNumber: number;
}
