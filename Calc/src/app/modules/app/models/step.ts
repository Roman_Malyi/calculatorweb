import {Option} from './option';

export class Step {
  public id: number;
  public name: string;
  public orderNumber: number;
  public isStepRoot: boolean;
  public stepOptions: Option[];
}
