import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { StepComponent } from './modules/app/components/step/step.component';
import { MainPageComponent } from './modules/app/components/main-page/main-page.component';

import { HttpClientModule } from '@angular/common/http';
import { StepsComponent } from './modules/app/components/steps/steps.component';
import { LastPageComponent } from './modules/app/components/last-page/last-page.component';

// import {Ng2PageScrollModule} from 'ng2-page-scroll';

@NgModule({
  declarations: [
    AppComponent,
    StepComponent,
    MainPageComponent,
    StepsComponent,
    LastPageComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // Ng2PageScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
